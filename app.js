// document.onmousedown = handleMouseDown;

// var grid = document.getElementById('pad-grid');
// grid.onmouseup = handleMouseUp;

// grid.onmouseleave = window.instrument.stopAllNotes;

window.onload = window.instrument.initialize1;
window.keyDown = [];
window.selectedController = 'pad-grid';

pads = document.getElementsByClassName('pad');
Array.from(pads, (pad) => {
	pad.onmousedown = handleMouseDown;
	pad.onmouseenter = handleMouseDown;
	pad.onmouseup = handleMouseUp;
	pad.onmouseleave = handleMouseUp;
});

// touchPad = document.getElementById('pad-touch');
// touchPad.onmousedown = handleMouseDown;
// touchPad.onmouseenter = handleMouseDown;
// touchPad.onmouseup = handleMouseUp;
// touchPad.onmouseleave = handleMouseUp;

document.onkeydown = handleKeyPress;
document.onkeyup = handleKeyRelease;

var selectedOctave = 5;
var selectedInstrument = 'synth';

function handleMouseUp(e) {
	switch (selectedController) {
		case 'pad-touch':
			convertPositionToNote(e);
			convertPositionToVelocity(e);
			dataPadStopAction(e);
			break;

		case 'pad-grid':
			if (e.buttons === 0 && e.type === 'mouseleave') return;
			if (e.target.matches('[data-pad]')) {
				dataPadStopAction(e.target.dataset.pad);
				return;
			}
			if (e.target.matches('[data-pad]')) {
				dataPadStopAction(e.target.dataset.pad);
			}
			break;
	}
}

function handleMouseDown(e) {
	if (e.buttons === 0) return;
	dataPadStartAction(e.target.dataset.pad);
}

function dataPadStartAction(padNr) {
	console.log(padNr);
	if (selectedInstrument === 'synth') {
		var midiValue = { pitch: ~~padNr + selectedOctave * 12 };
		window.instrument.playNote(midiValue);
	}
}

function dataPadStopAction(padNr) {
	if (selectedInstrument === 'synth') {
		var midiValue = { pitch: ~~padNr + selectedOctave * 12 };
		window.instrument.stopNote(midiValue);
	}
}

function handleKeyPress(e) {
	if (window.keyNumbers[e.key] == null || window.keyDown[e.key] != null) return;
	window.keyDown[e.key] = true;
	console.log(`keydown ${e.key}`);

	keyNr = window.keyboardMap[e.key];

	if (selectedInstrument === 'synth') {
		var midiValue = { pitch: keyNr + selectedOctave * 12 };
		window.instrument.playNote(midiValue);
	}
}
function handleKeyRelease(e) {
	if (window.keyNumbers[e.key] == null || window.keyDown[e.key] == null) return;
	window.keyDown[e.key] = null;
	console.log(`keyup ${e.key}`);

	keyNr = window.keyboardMap[e.key];
	if (selectedInstrument === 'synth') {
		var midiValue = { pitch: keyNr + selectedOctave * 12 };
		window.instrument.stopNote(midiValue);
	}
}
