audioContext = new (window.AudioContext || window.webkitAudioContext)();
let oscList = [];
let mainGainNode = null;

let oscGain = 0.5;
let mainGain = 0.5;

let activeOscs = 0;

mainGainNode = audioContext.createGain();
mainGainNode.connect(audioContext.destination);
mainGainNode.gain.value = mainGain;

for (i = 0; i < 127; i++) {
	oscList[i] = [];
}

function playNote(midiValue) {
	// console.log(`start note: ${midiValue.pitch}`);

	var oscillators = [];
	var osc = configureOscillator({ type: 'sine', pitch: window.noteValues[midiValue.pitch] });
	oscillators.push(osc);
	// var osc = configureOscillator({ type: 'square', pitch: window.noteValues[midiValue.pitch] - 1 });
	// oscillators.push(osc);
	// var osc = configureOscillator({ type: 'square', pitch: window.noteValues[midiValue.pitch] - 1 });
	// oscillators.push(osc);
	// var osc = configureOscillator({ type: 'sine', pitch: window.noteValues[midiValue.pitch] - 2 });
	// oscillators.push(osc);
	// var osc = configureOscillator({ type: 'sine', pitch: window.noteValues[midiValue.pitch] + 2 });
	// oscillators.push(osc);

	oscillators.forEach((oscillator) => oscillator.start(audioContext.currentTime));

	oscList[midiValue.pitch].push(oscillators);

	activeOscs++;
	mainGainNode.gain.value = mainGain / (activeOscs == 0 ? 1 : activeOscs);
}

function stopNote(midiValue) {
	// console.log(`stop note: ${midiValue.pitch}`);
	var oscillators = oscList[midiValue.pitch].shift();
	oscillators.forEach((osc) => {
		var gain = osc.gain;
		gain.gain.setValueAtTime(oscGain, audioContext.currentTime); //why is this necessary for rampToValue to work? who knows.
		gain.gain.exponentialRampToValueAtTime(0.0001, audioContext.currentTime + 0.1); //ramp down to 0.0001, because 0 is not allowed in chrome's exponential functions
		setTimeout(() => osc.stop(), 200); //200 appearantly a sweetspot for getting rid of clicks
	});
	// console.log(`length: ${oscList.length}`);
	// delete oscList[midiValue.pitch];
	activeOscs--;
}

function stopAllNotes() {
	oscList.forEach((note) => note.stop());
	oscList = [];
}

function configureOscillator(params) {
	var oscillator = audioContext.createOscillator();
	oscillator.type = params.type;
	oscillator.frequency.value = params.pitch;

	var oscillatorGain = audioContext.createGain();
	oscillatorGain.gain.value = oscGain;

	oscillator.connect(oscillatorGain);
	oscillatorGain.connect(mainGainNode);
	oscillator.gain = oscillatorGain;
	return oscillator;
}

this.window.instrument = { playNote, stopNote };
